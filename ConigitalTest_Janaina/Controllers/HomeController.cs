﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConigitalTest_Janaina.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int input)
        {
            string multiple7 = input % 7 == 0 ? "S" : "N";

            string multiple9 = input % 9 == 0 ? "S" : "N";

            if(multiple7.Equals("S") && multiple9.Equals("S"))
                return Json(new { number = input, result = "SN"  }, JsonRequestBehavior.AllowGet);
            else
                if(multiple7.Equals("S"))
                    return Json(new { number = input, result = "S" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { number = input, result = "N" }, JsonRequestBehavior.AllowGet);
        }
    }
}